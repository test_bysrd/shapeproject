/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suraida.shapeproject;

/**
 *
 * @author sulaiman
 */
public class Rectangle {
    private double w;
    private double h;
    public Rectangle(double w,double h){
        this.w = w;
        this.h = h;
    }
    public double calArea(){
        return w * h;
    }
    public double getW(){
        return w;
    }
    public double getH(){
        return h;
    }
    public void setWH(double w ,double h){
        if(w<=0) {
            System.out.println("Error: width must more than zero!!!");
            return;
        }
        if(h<=0) {
            System.out.println("Error: height must more than zero!!!");
            return;
        }
        this.w = w;
        this.h = h;
    }
}
